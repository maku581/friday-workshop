//
//  Friday_WorkshopTests.swift
//  Friday WorkshopTests
//
//  Created by Manoj Ashok Kurien on 27/04/2018.
//  Copyright © 2018 Manoj Ashok Kurien. All rights reserved.
//

import XCTest
@testable import Friday_Workshop

class Friday_WorkshopTests: XCTestCase {
    
    let dataProvider = DataProvider()
    
    override func setUp() {
        super.setUp()
        dataProvider.setMyData( MyData(name: "Jim", email: "Jim@email.com", notes: "test notes"))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testGetMyName() {
        let name = dataProvider.getMyData().name
        XCTAssertEqual("Jim", name)
    }
    
    func testGetMyEmail()  {
        let name = dataProvider.getMyData().email
        XCTAssertEqual("Jim@email.com", name)
    }
    
    func testGetMyNotes()  {
        let notes = dataProvider.getMyData().notes
        XCTAssertEqual("test notes", notes)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
