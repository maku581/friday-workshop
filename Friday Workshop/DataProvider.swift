//
//  DataProvider.swift
//  Friday Workshop
//
//  Created by Manoj Ashok Kurien on 27/04/2018.
//  Copyright © 2018 Manoj Ashok Kurien. All rights reserved.
//

import Foundation

class DataProvider {
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("mydata")
    
    func setMyData(_ myData:MyData){
        NSKeyedArchiver.archiveRootObject(myData, toFile: DataProvider.ArchiveURL.path)
    }
    
    func getMyData() -> MyData {
        if let data = NSKeyedUnarchiver.unarchiveObject(withFile: DataProvider.ArchiveURL.path) as? MyData{
            return data
        }
        else{
            print("Error, returning empty data")
            return MyData(name: "", email: "", notes: "")
        }
    }
}

struct MyDataKeys {
    static let name = "name"
    static let email = "email"
    static let notes = "notes"
}


class MyData: NSObject, NSCoding  {
    var name:String, email:String, notes:String
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: MyDataKeys.name)
        aCoder.encode(email, forKey: MyDataKeys.email)
        aCoder.encode(notes, forKey: MyDataKeys.notes)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        guard let decodedName = aDecoder.decodeObject(forKey: MyDataKeys.name) as? String,
            let decodedEmail = aDecoder.decodeObject(forKey: MyDataKeys.email)  as? String,
            let decodedNotes = aDecoder.decodeObject(forKey: MyDataKeys.notes)  as? String
            else {
                print (" Error")
                return nil
        }
        
        self.init(name:decodedName, email:decodedEmail, notes:decodedNotes)
    }
    
    init(name: String, email: String, notes: String){
        self.name = name ; self.email = email ; self.notes = notes
    }
}




