//
//  ViewController.swift
//  Friday Workshop
//
//  Created by Manoj Ashok Kurien on 27/04/2018.
//  Copyright © 2018 Manoj Ashok Kurien. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeMyDetails(_ sender: UIButton) {
        let storyboard = UIStoryboard(name : "ChangeMyDetails", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChangeMyDetailsView")
        
        guard let navigationController = self.navigationController else {
            print("navigation controller not present")
            return
        }
    
        navigationController.pushViewController(viewController, animated: true)
        print("all good")
    }
    
}

